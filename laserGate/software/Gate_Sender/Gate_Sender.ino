#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>

#define COOL_DOWN 2000
#define LIMIT_LOW 300
#define LIMIT_HIGH 800
#define MINIMUM_LEVEL 800

#define RADIO_CE 9
#define RADIO_CSN 10
#define RADIO_IRQ 8

#define BUZZER 4
#define PHOTODIODE A0

#define LED_GREEN 2
#define LED_RED 3

RF24 radio(RADIO_CE, RADIO_CSN);
const byte address[6] = "00001";
const char sender = 'A';
int value;
bool ready = false;

void setup() {
  pinMode(BUZZER, OUTPUT);
  pinMode(LED_GREEN, OUTPUT);
  pinMode(LED_RED, OUTPUT);
  pinMode(PHOTODIODE, INPUT);
  analogReference(EXTERNAL); // 3.3V regulator

  Serial.begin(9600);

  radio.begin();
  radio.openWritingPipe(address);
  radio.setPALevel(RF24_PA_MIN);
  radio.stopListening();

  // Selftest !?!?!?!
  for (int i = 0; i < 12; i++) {
    digitalWrite(LED_GREEN, LOW);
    digitalWrite(LED_RED, HIGH);
    delay(150);
    digitalWrite(LED_GREEN, HIGH);
    digitalWrite(LED_RED, LOW);
    delay(150);
    void beep();
  }

  digitalWrite(LED_RED, HIGH); // ON indicator
  digitalWrite(LED_GREEN, LOW);
}

void beep() {
  digitalWrite(BUZZER, HIGH);
  delay(30);
  digitalWrite(BUZZER, LOW);
}

void loop() {
  value = analogRead(PHOTODIODE);
  if ( value > MINIMUM_LEVEL ) {
    ready = true;
    digitalWrite(LED_GREEN, HIGH);
  }

  if (ready == true && value < LIMIT_LOW) {
    radio.write(&sender, sizeof(sender));
    beep();
    ready = false;
    digitalWrite(LED_GREEN, LOW);
    delay(COOL_DOWN); // cool down periode, indtil Gate kan aktiveres igen
  }
  delay(10);
}

int adcToVoltage(int value){
  return 3300*(value/1023.0);
}

void debugToPC(int value){
  Serial.println( value );
  Serial.print("miliVolt: ");
  Serial.println( adcToVoltage( value ) );
  Serial.println("----");
}
