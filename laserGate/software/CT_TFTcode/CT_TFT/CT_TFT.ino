#include <UTFT.h>
#include <URTouch.h>
#include <memorysaver.h>




// Declare which fonts we will be using
extern uint8_t BigFont[];

UTFT myGLCD(ITDB32S,38,39,40,41);
//UTFT myGLCD(ILI9341_S5P,11,12,13,10);
URTouch myTouch(6, 5, 4, 3, 2);

int interruptPin = 2;
int xmax, ymax;
int offset = 0;
const int nOT = 8; // number of timestamps

long timeStamps[nOT];

typedef struct  {
  int x1; // upper left corner
  int y1;
  int x2; // lower right corner
  int y2;
} BTN;

BTN btn1 = {2,200,144,239};
BTN btn2 = {152,200,319,239};

void setup()
{
  randomSeed(analogRead(0));
  
  Serial.begin(9600);
// Setup the LCD
  myGLCD.InitLCD();
  myGLCD.setBrightness(4);
  myGLCD.setFont(BigFont);

  myTouch.InitTouch();
  myTouch.setPrecision(PREC_MEDIUM);

  xmax = myGLCD.getDisplayXSize();
  ymax = myGLCD.getDisplayYSize();
  myGLCD.clrScr();
  myGLCD.setColor(255, 0, 0);
  drawButton( &btn1 );
  myGLCD.setColor(0, 255, 0);
  drawButton( &btn2 );
  myGLCD.setColor(0xFF, 0xFF, 0xFF);

  //attachInterrupt( digitalPinToInterrupt(interruptPin), blink, CHANGE );
}


void blink(){}

void loop()
{
  int buf[318];
  int tx;
  int ty;

  if (myTouch.dataAvailable()){
      //Serial.println("data available");
      myTouch.read();
      tx=myTouch.getX();  // touch x
      ty=myTouch.getY();  // touch y
      Serial.println("--------");
      Serial.print( tx );
      Serial.print(", ");
      Serial.println( ty );


      if ( collision( tx, ty, &btn1 )){
        Serial.println( "Button1" );
      }
      if ( collision( tx, ty, &btn2 )){
        Serial.println( "Button2" );
      }
  }
// Clear the screen and draw the frame



 // myGLCD.print("1) 00:00:00",1, offset+=22);
  //myGLCD.setFont(uint8_t *font)
  delay(100);
}

bool collision(int x, int y, BTN *btn){
  if ( x > btn->x1 && y > btn->y1 ){
    if ( x < btn->x2 && y < btn->y2 )
      return true;
  }
  return false;
}

void drawButton(BTN *btn){
  myGLCD.fillRoundRect(btn->x1, btn->y1, btn->x2, btn->y2);
}


