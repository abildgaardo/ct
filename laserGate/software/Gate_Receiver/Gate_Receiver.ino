#include <Wire.h>
#include <LCD_I2C.h>

#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>

#define RADIO_CE 6
#define RADIO_CSN 4
#define BTN_RED 2
#define BTN_YELLOW 3
#define BTN_GREEN 4

// hd44780_I2Cexp lcd;  // declare lcd object: auto locate & auto config expander chip
LCD_I2C lcd(0x3F, 16, 4);

const int LCD_COLS = 16;
const int LCD_ROWS = 4;

RF24 radio(RADIO_CE, RADIO_CSN);
const byte address[6] = "00001";
char sender;

#define STATE_READY 0
#define STATE_RUNNING 1
int state;
unsigned long startTime;      // milliseconds
unsigned long stopTime;       // milliseconds
unsigned long nextLcdUpdate;  // milliseconds
unsigned long currentTime;    // milliseconds
float runTime;                // seconds
float elapsedTime;            // seconds

void setup() {
  pinMode(BTN_RED, INPUT_PULLUP);
  pinMode(BTN_YELLOW, INPUT_PULLUP);
  pinMode(BTN_RED, INPUT_PULLUP);

  Serial.begin(9600);

  lcd.begin();
  lcd.backlight();

  radio.begin();
  radio.openReadingPipe(0, address);
  radio.setPALevel(RF24_PA_MIN);
  radio.startListening();

  // Selftest !?!?!?!
  lcd.print("CT LaserGate HTX");
  lcd.setCursor(0, 1);
  lcd.print("Version 0.1b");

  lcd.clear();
  state = STATE_READY;
  lcd.print("Ready");
  startTime = 0;
  stopTime = 0;
  elapsedTime = 0;
  nextLcdUpdate = 0;
}

void beep() {
  //digitalWrite(BUZZER, HIGH);
  delay(20);
  //digitalWrite(BUZZER, LOW);
}

void loop() {
  if (radio.available())  //Looking for the data.
  {
    radio.read(&sender, sizeof(sender));  //Reading the data
    if (sender == 'A') {
      startTime = millis();
      stopTime = 0;
      beep();
      lcd.setCursor(0, 0);
      lcd.print("Running :       ");
      elapsedTime = 0;
      state = STATE_RUNNING;
      //int dummy = analogRead(PHOTODIODE); // This will fuck up futher radio com, dont know why???
    }
    Serial.println(sender);
  }

  if (state == STATE_RUNNING) {
    if (analogRead(PHOTODIODE) < 100) {
      stopTime = millis();
      runTime = (stopTime - startTime) / 1000.0;
      beep();
      lcd.setCursor(0, 1);
      lcd.print("Time    :       ");
      lcd.setCursor(10, 1);
      if (runTime < 10.0) {
        lcd.print(" ");
      }
      lcd.print(runTime);
      lcd.setCursor(0, 0);
      lcd.print("Ready           ");
      state = STATE_READY;
    } else {
      currentTime = millis();
      if (nextLcdUpdate < currentTime) {
        elapsedTime = (currentTime - startTime) / 1000.0;
        if (elapsedTime < 90.0) {
          lcd.setCursor(10, 0);
          if (elapsedTime < 10.0) {
            lcd.print(" ");
          }
          lcd.print(elapsedTime);
          nextLcdUpdate = currentTime + 383;  // This is af magic number
        } else {
          beep();
          lcd.setCursor(0, 0);
          lcd.print("Ready           ");
          state = STATE_READY;
        }
      }
    }
  }
}