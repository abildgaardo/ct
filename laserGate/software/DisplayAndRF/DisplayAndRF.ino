#include <Wire.h>
#include <LCD_I2C.h>

#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>
#include "custom_char.h"
/*
GPIO Pin Number	nRF24L01+ SPI
D9	-> CE (Chip Enable)
D10	-> CS/CSN (Chip Select)
D11	-> MOSI
D12	-> MISO
D13	-> SCK
*/
#define RADIO_CE 9
#define RADIO_CSN 10
#define BUZZER A3
#define LASER_PWM 3
//=== buttons =============================================
#define BTN_GREEN 2
#define BTN_YELLOW 4
#define BTN_RED 3
//=========================================================
#define NUM_STAMPS 5

enum State {
  READY=0,   // Waiting for _FIRST_ trigger to start timer (FLAG IS UP)
  RUNNING=3, // Timer is running ! listening for triggers  (HEART IS ON)
  PAUSED=2,  // stop timer, but dont reset.
  STOPPED=1  // Stop and reset
};

char* disp[] = {"--:--:---"};

LCD_I2C lcd(0x3F, 20, 4);

RF24 radio(RADIO_CE, RADIO_CSN);
const byte address[6] = "00001";
char sender;
uint8_t btn_status = 0;

State state = READY;

uint8_t index = 0;
unsigned long timestamp[NUM_STAMPS]; 



uint8_t buttonScan(){
  if ( !digitalRead(BTN_RED) ) return BTN_RED;
  if ( !digitalRead(BTN_YELLOW) ) return BTN_YELLOW;
  if ( !digitalRead(BTN_GREEN) ) return BTN_GREEN;
  return 0;
}



void updateDisplay(){
  for(int x=1;x<index;x++){
    getTimeString(timestamp[0], timestamp[x]);
    lcd.setCursor(0,x-1);
    lcd.print( *disp );
  }
}

void displayStatus(State t){
  lcd.setCursor(20,0);
  lcd.write(t);

}

void getTimeString(unsigned long t1, unsigned long t2){
  unsigned long sec;
  unsigned long min;
  unsigned long milis;
  unsigned long diff;
  
  diff = t2 - t1;
   sec = diff / 1000;
  min = sec / 60;
  milis = diff % 1000;
  sec %= 60;
  min %= 60;

  sprintf(*disp, "%02d:%02d:%03d", (int)min, (int)sec, (int)milis);
}


void clearTimestamps(){
  for(int x=0;x<NUM_STAMPS;x++){
    timestamp[x] = 0;
  }
  index = 0;
}

void setup() {

  pinMode(BTN_GREEN, INPUT_PULLUP);
  pinMode(BTN_YELLOW, INPUT_PULLUP);
  pinMode(BTN_RED, INPUT_PULLUP);

  Serial.begin(9600);

  lcd.begin();
  lcd.createChar(0, c1);  // READY (FLAG) 
  lcd.createChar(1, c10); // STOP
  lcd.createChar(2, c9);  // PAUSE
  lcd.createChar(3, c8);  // RUNNING (PLAY ICON)
  lcd.backlight();

  radio.begin();
  radio.openReadingPipe(0, address);
  radio.setPALevel(RF24_PA_MIN);
  radio.startListening();
  radio.flush_rx();
  //bool result = 
  Serial.print("RadioConnected: ");
  Serial.println( radio.isChipConnected () );
  delay(1000);

  // Selftest !?!?!?!
  lcd.print("  CT LaserGate HTX");
  lcd.setCursor(0, 2);
  lcd.print("    Version 0.1b");
  delay(1000);
  lcd.clear();
clearTimestamps();
  state = READY;
  displayStatus(READY);
}





void loop() {
  if (radio.available())  //Looking for the data.
  {
    Serial.println("Data on RF");
    radio.read(&sender, sizeof(sender));  //Reading the data
    if (sender == 'A') {
      
      if (state == RUNNING) {
        timestamp[index++] = millis();
        if (index>=NUM_STAMPS){ // STOPPED AFTER MAX stamps is reached.. (e.g 4)
          displayStatus(STOPPED);
          state = STOPPED;
        }
        Serial.println("RF triggered.");
        updateDisplay();
      } else
      if (state == READY){ // Hvis ready så start timer.. (dvs. sæt første timestamp)
        timestamp[index++] = millis();
        state =  RUNNING;
        displayStatus(RUNNING);
        updateDisplay();
      }
    }
    radio.flush_rx();
  }
  btn_status = buttonScan();
  if( btn_status ){
    delay(100);
    if (btn_status){

      if ( state == STOPPED ){
          if ( btn_status == BTN_RED ){
            state = STOPPED;
            clearTimestamps();
            lcd.clear();
            displayStatus(STOPPED);
          }
          if ( btn_status == BTN_GREEN ){
            state = READY;  
            displayStatus(READY);
          }
      } else if ( state == RUNNING ) {
        if ( btn_status == BTN_RED ){
          state = STOPPED;
          displayStatus(STOPPED);
        }

      } else if ( state == READY ) {
        if ( btn_status == BTN_RED ){
          state = STOPPED;
          displayStatus(STOPPED);
        }
        if ( btn_status == BTN_YELLOW ){
          state = PAUSED;  
          displayStatus(PAUSED);
        }
      }
      else if ( state == PAUSED ) {
          if ( btn_status == BTN_GREEN ){
            state = READY;  
            displayStatus(READY);
          }
      }

      }
      Serial.println( btn_status );
    }
  }



