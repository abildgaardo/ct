#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>

#define RADIO_CE 10
#define RADIO_CSN 9
#define BTN_SEND 7
#define LED_STATUS 2

RF24 radio(RADIO_CE, RADIO_CSN); // CS, CSN
const byte address[6] = "00001";
const char sender = 'A';
int value;
bool sendTestPacket = false;

void setup() {
  pinMode(BTN_SEND, INPUT_PULLUP);
  pinMode(LED_STATUS, OUTPUT);
  digitalWrite(LED_STATUS, LOW);

  Serial.begin(9600);

  radio.begin();
  radio.openWritingPipe(address);
  radio.setPALevel(RF24_PA_MIN);
  radio.stopListening();


  Serial.print("RadioConnected: ");
  Serial.println( radio.isChipConnected () );
  delay(2000);
  digitalWrite(LED_STATUS, HIGH);
  delay(1000);
  digitalWrite(LED_STATUS, LOW);
}


void loop() {
 
  if ( !digitalRead(BTN_SEND) ){
    delay(100);
    if ( !digitalRead(BTN_SEND) ){
      Serial.println("BUTTON_PRESSED");
      sendTestPacket = true;
    }
  }

  if ( sendTestPacket ) {
    radio.write(&sender, sizeof(sender));
    sendTestPacket = false;
    digitalWrite(2, HIGH);
    delay(500); // cool down periode, indtil Gate kan aktiveres igen
    digitalWrite(2, LOW);
  }
}